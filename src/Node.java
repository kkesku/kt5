import java.util.*;

public class Node {

   /*
    * Solution insipred from https://pastebin.com/trMY6MxL
    * */

   private String name;
   private Node firstChild;
   private Node nextSibling;
   private int info;

   Node (String n, Node d, Node r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }


   @Override
   public String toString() {
      return "Node{" +
              "name='" + name + '\'' +
              ", info=" + info +
              ", firstChild=" + firstChild +
              ", nextSibling=" + nextSibling +
              '}';
   }

   public void correctInput(Node root, String s){
      Node check = root;
      if (root.name == null || root.name.contains(" ")){
         throw new RuntimeException("Wrong input " + s);
      }
      while (check.firstChild != null){
         if (check.firstChild.name == null){
            throw new RuntimeException("Wrong input " + s);
         }
         check = check.firstChild;
         while (check.nextSibling != null){
            if(check.nextSibling.name == null){
               throw new RuntimeException("Wrong input " + s);
            }
            check = check.nextSibling;
         }
      }
   }

   public static Node parsePostfix (String s) {
      Node root = new Node(null, null, null);
      root.info = 1;
      StringTokenizer string = new StringTokenizer(s.trim(), "(),", true);
      Stack<Node> stack = new Stack<>();

      while (string.hasMoreTokens()){
         String cur = string.nextToken();
         if(cur.equals("(")){
            stack.push(root);
            root.firstChild = new Node(null,null, null);
            root.firstChild.info = stack.size() + 1;
            root = root.firstChild;
         }
         else if(cur.equals(")")){
            root = stack.pop();
         }
         else if (cur.equals(",")){
            if(stack.empty()){
               throw new RuntimeException("Wrong input " + s);
            }
            root.nextSibling = new Node(null, null, null);
            root.nextSibling.info = stack.size() + 1;
            root = root.nextSibling;
         }
         else {
            root.name = cur;
         }
      }
      root.correctInput(root, s);
      return root;
   }

   public String leftParentheticRepresentation() {
      StringBuilder builder = new StringBuilder();
      builder.append(this.name);
      if (this.firstChild != null) {
         builder.append("(");
         builder.append(this.firstChild.leftParentheticRepresentation());
         builder.append(")");
      }

      if (this.nextSibling != null) {
         builder.append(",");
         builder.append(this.nextSibling.leftParentheticRepresentation());
      }
      return builder.toString();
   }

   public String pseudoXML() {
      StringBuilder builder = new StringBuilder();
      StringBuilder str = new StringBuilder();
      str.append("\t".repeat(Math.max(0, this.info - 1)));
      String r = str + "\t";
      builder.append(str).append("<L").append(this.info).append("> ").append(this.name).append(" ");

      if (this.firstChild != null) {
         builder.append("\n");
         builder.append(this.firstChild.pseudoXML());
         builder.append(r).append("</L").append(this.info + 1).append("> \n");
      }

      if (this.nextSibling != null) {
         builder.append(str).append("</L").append(this.info).append("> \n");
         builder.append(this.nextSibling.pseudoXML());
      }

      if(this.info == 1){
         builder.append("</L1>");
      }
      return builder.toString().replaceAll(" [\t]+</", " </");
   }

   public static void main (String[] param) {
      String s = "(B1,C)A";
      Node t = Node.parsePostfix (s);
//      String v = t.leftParentheticRepresentation();
//      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)

      String k = "A(B(C(D(E))))";
      Node l = Node.parsePostfix(k);
      System.out.println(l.pseudoXML());
//      System.out.println(l.leftParentheticRepresentation());
//      String ws = "(((G,H)D,E,(I)F)B,(J)C)A";
//      Node m = Node.parsePostfix(ws);
//      System.out.println(m.leftParentheticRepresentation());
   }
}