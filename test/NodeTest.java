
import static org.junit.Assert.*;
import org.junit.Test;
// import java.util.*;

/** Testklass.
 * @author Jaanus
 */
public class NodeTest {

   @Test (timeout=1000)
   public void testParsePostfix() { 
      String s = "(B1,C,D)A";
      Node t = Node.parsePostfix (s);
      String r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "A(B1,C,D)", r);
      s = "(((2,1)-,4)*,(69,3)/)+";
      t = Node.parsePostfix (s);
      r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "+(*(-(2,1),4),/(69,3))", r);
   }

   @Test (timeout=1000)
   public void testParsePostfixAndLeftParentheticRepresentation1() {
      String s = "((((E)D)C)B)A";
      Node t = Node.parsePostfix (s);
      String r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "A(B(C(D(E))))", r);
      s = "((C,(E)D)B,F)A";
      t = Node.parsePostfix (s);
      r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "A(B(C,D(E)),F)", r);
   }

   @Test (timeout=1000)
   public void testParsePostfixAndLeftParentheticRepresentation2() {
      String s = "(((512,1)-,4)*,(-6,3)/)+";
      Node t = Node.parsePostfix (s);
      String r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "+(*(-(512,1),4),/(-6,3))", r);
      s = "(B,C,D,E,F)A";
      t = Node.parsePostfix (s);
      r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "A(B,C,D,E,F)", r);
      s = "((1,(2)3,4)5)6";
      t = Node.parsePostfix (s);
      r = t.leftParentheticRepresentation();
       assertEquals ("Tree: " + s, "6(5(1,3(2),4))", r);
   }

   @Test (timeout=1000)
   public void testSingleRoot() {
      String s = "ABC";
      Node t = Node.parsePostfix (s);
      String r = t.leftParentheticRepresentation();
      assertEquals ("Tree: " + s, "ABC", r);
      s = ".Y.";
      t = new Node (s, null, null);
      r = t.leftParentheticRepresentation();
      assertEquals ("Single node" + s, s, r);
   }

   @Test (timeout = 1000)
   public void testSingleRootXML(){
      String s = "ABC";
      Node root = Node.parsePostfix(s);
      String result = root.pseudoXML();
      assertEquals("<L1> ABC </L1>", result);
   }

   @Test (timeout = 1000)
   public void testExampleXML(){
      String s = "((C)B,(E,F)D,G)A";
      Node root = Node.parsePostfix(s);
      String result = root.pseudoXML();
      assertEquals("""
              <L1> A\s
              \t<L2> B\s
              \t\t<L3> C </L3>\s
              \t</L2>\s
              \t<L2> D\s
              \t\t<L3> E </L3>\s
              \t\t<L3> F </L3>\s
              \t</L2>\s
              \t<L2> G </L2>\s
              </L1>""", result);
   }

   @Test (timeout = 1000)
   public void testXML(){
      String s = "+(*(-(512,1),4),/(-6,3))";
      Node root = Node.parsePostfix(s);
      String result = root.pseudoXML();
      assertEquals("""
              <L1> +\s
              \t<L2> *\s
              \t\t<L3> -\s
              \t\t\t<L4> 512 </L4>\s
              \t\t\t<L4> 1 </L4>\s
              \t\t</L3>\s
              \t\t<L3> 4 </L3>\s
              \t</L2>\s
              \t<L2> /\s
              \t\t<L3> -6 </L3>\s
              \t\t<L3> 3 </L3>\s
              \t</L2>\s
              </L1>""", result);
   }

   @Test (timeout = 1000)
   public void testSingleChildren(){
      String s = "(B1)A";
      Node root = Node.parsePostfix(s);
      String result = root.pseudoXML();
      assertEquals("""
              <L1> A\s
              \t<L2> B1 </L2>\s
              </L1>""", result);
   }
   @Test (timeout = 1000)
   public void testOnlyChildren(){
      String s = "A(B(C(D(E))))";
      Node root = Node.parsePostfix(s);
      String result = root.pseudoXML();
      assertEquals("""
              <L1> A\s
              \t<L2> B\s
              \t\t<L3> C\s
              \t\t\t<L4> D\s
              \t\t\t\t<L5> E </L5>\s
              \t\t\t</L4>\s
              \t\t</L3>\s
              \t</L2>\s
              </L1>""", result);
   }

   @Test (expected=RuntimeException.class)
   public void testSpaceInNodeName() {
      Node root = Node.parsePostfix ("A B");
   }

   @Test (expected=RuntimeException.class)
   public void testTwoCommas() {
      Node t = Node.parsePostfix ("(B,,C)A");
   }

   @Test (expected=RuntimeException.class)
   public void testEmptySubtree() {
      Node root = Node.parsePostfix ("()A");
   }

   @Test (expected=RuntimeException.class)
   public void testInputWithBracketsAndComma() {
      Node t = Node.parsePostfix ("( , ) ");
   }

   @Test (expected=RuntimeException.class)
   public void testInputWithoutBrackets() {
      Node t = Node.parsePostfix ("A,B");
   }

   @Test (expected=RuntimeException.class)
   public void testInputWithDoubleBrackets() {
      Node t = Node.parsePostfix ("((C,D))A");
   }

   @Test (expected=RuntimeException.class)
   public void testComma1() {
      Node root = Node.parsePostfix ("(,B)A");
   }

   @Test (expected=RuntimeException.class)
   public void testComma2() {
      Node root = Node.parsePostfix ("(B)A,(D)C");
   }

   @Test (expected=RuntimeException.class)
   public void testComma3() {
      Node root = Node.parsePostfix ("(B,C)A,D");
   }

   @Test (expected=RuntimeException.class)
   public void testTab1() {
      Node root = Node.parsePostfix ("\t");
   }

   @Test (expected=RuntimeException.class)
   public void testWeirdBrackets() {
      Node root = Node.parsePostfix (")A(");
   }

}

